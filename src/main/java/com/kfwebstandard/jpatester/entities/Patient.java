package com.kfwebstandard.jpatester.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ken
 */
@Entity
@Table(name = "patient", catalog = "HOSPITALDB", schema = "")
@NamedQueries({
    @NamedQuery(name = "Patient.findAll", query = "SELECT p FROM Patient p"),
    @NamedQuery(name = "Patient.findByPatientid", query = "SELECT p FROM Patient p WHERE p.patientid = :patientid"),
    @NamedQuery(name = "Patient.findByLastname", query = "SELECT p FROM Patient p WHERE p.lastname = :lastname"),
    @NamedQuery(name = "Patient.findByFirstname", query = "SELECT p FROM Patient p WHERE p.firstname = :firstname"),
    @NamedQuery(name = "Patient.findByDiagnosis", query = "SELECT p FROM Patient p WHERE p.diagnosis = :diagnosis"),
    @NamedQuery(name = "Patient.findByAdmissiondate", query = "SELECT p FROM Patient p WHERE p.admissiondate = :admissiondate"),
    @NamedQuery(name = "Patient.findByReleasedate", query = "SELECT p FROM Patient p WHERE p.releasedate = :releasedate")})
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PATIENTID")
    private Integer patientid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LASTNAME")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "DIAGNOSIS")
    private String diagnosis;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADMISSIONDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date admissiondate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RELEASEDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date releasedate;
    @OneToMany(mappedBy = "patientid")
    private List<Inpatient> inpatientList;
    @OneToMany(mappedBy = "patientid")
    private List<Surgical> surgicalList;
    @OneToMany(mappedBy = "patientid")
    private List<Medication> medicationList;

    public Patient() {
    }

    public Patient(Integer patientid) {
        this.patientid = patientid;
    }

    public Patient(Integer patientid, String lastname, String firstname, String diagnosis, Date admissiondate, Date releasedate) {
        this.patientid = patientid;
        this.lastname = lastname;
        this.firstname = firstname;
        this.diagnosis = diagnosis;
        this.admissiondate = admissiondate;
        this.releasedate = releasedate;
    }

    public Integer getPatientid() {
        return patientid;
    }

    public void setPatientid(Integer patientid) {
        this.patientid = patientid;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Date getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(Date admissiondate) {
        this.admissiondate = admissiondate;
    }

    public Date getReleasedate() {
        return releasedate;
    }

    public void setReleasedate(Date releasedate) {
        this.releasedate = releasedate;
    }

    public List<Inpatient> getInpatientList() {
        return inpatientList;
    }

    public void setInpatientList(List<Inpatient> inpatientList) {
        this.inpatientList = inpatientList;
    }

    public List<Surgical> getSurgicalList() {
        return surgicalList;
    }

    public void setSurgicalList(List<Surgical> surgicalList) {
        this.surgicalList = surgicalList;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patientid != null ? patientid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.patientid == null && other.patientid != null) || (this.patientid != null && !this.patientid.equals(other.patientid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.kenfogel.jpatester.entities.Patient[ patientid=" + patientid + " ]";
    }

}
