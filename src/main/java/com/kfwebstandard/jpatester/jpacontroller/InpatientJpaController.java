package com.kfwebstandard.jpatester.jpacontroller;

import com.kfwebstandard.jpatester.entities.Inpatient;
import com.kfwebstandard.jpatester.entities.Patient;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.NonexistentEntityException;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generated controller updated to use CDI
 *
 * @author Ken
 */
@Named
@RequestScoped
public class InpatientJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(InpatientJpaController.class);

    // Required for CDI
    @PersistenceContext(unitName = "hospitalPU")
    private EntityManager em;

    // Required for CDI
    @Resource
    private UserTransaction utx;

    public void create(Inpatient inpatient) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Patient patientid = inpatient.getPatientid();
            if (patientid != null) {
                patientid = em.getReference(patientid.getClass(), patientid.getPatientid());
                inpatient.setPatientid(patientid);
            }
            em.persist(inpatient);
            if (patientid != null) {
                patientid.getInpatientList().add(inpatient);
                patientid = em.merge(patientid);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public void edit(Inpatient inpatient) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Inpatient persistentInpatient = em.find(Inpatient.class, inpatient.getId());
            Patient patientidOld = persistentInpatient.getPatientid();
            Patient patientidNew = inpatient.getPatientid();
            if (patientidNew != null) {
                patientidNew = em.getReference(patientidNew.getClass(), patientidNew.getPatientid());
                inpatient.setPatientid(patientidNew);
            }
            inpatient = em.merge(inpatient);
            if (patientidOld != null && !patientidOld.equals(patientidNew)) {
                patientidOld.getInpatientList().remove(inpatient);
                patientidOld = em.merge(patientidOld);
            }
            if (patientidNew != null && !patientidNew.equals(patientidOld)) {
                patientidNew.getInpatientList().add(inpatient);
                patientidNew = em.merge(patientidNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = inpatient.getId();
                if (findInpatient(id) == null) {
                    throw new NonexistentEntityException("The inpatient with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Inpatient inpatient;
            try {
                inpatient = em.getReference(Inpatient.class, id);
                inpatient.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inpatient with id " + id + " no longer exists.", enfe);
            }
            Patient patientid = inpatient.getPatientid();
            if (patientid != null) {
                patientid.getInpatientList().remove(inpatient);
                patientid = em.merge(patientid);
            }
            em.remove(inpatient);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Inpatient> findInpatientEntities() {
        return findInpatientEntities(true, -1, -1);
    }

    public List<Inpatient> findInpatientEntities(int maxResults, int firstResult) {
        return findInpatientEntities(false, maxResults, firstResult);
    }

    private List<Inpatient> findInpatientEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Inpatient.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Inpatient findInpatient(Integer id) {
        return em.find(Inpatient.class, id);
    }

    public int getInpatientCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Inpatient> rt = cq.from(Inpatient.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
