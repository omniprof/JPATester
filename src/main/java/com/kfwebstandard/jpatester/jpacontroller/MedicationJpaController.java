package com.kfwebstandard.jpatester.jpacontroller;

import com.kfwebstandard.jpatester.entities.Medication;
import com.kfwebstandard.jpatester.entities.Patient;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.NonexistentEntityException;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generated controller updated to use CDI
 *
 * @author Ken
 */
@Named
@RequestScoped
public class MedicationJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(MedicationJpaController.class);

    // Required for CDI
    @PersistenceContext(unitName = "hospitalPU")
    private EntityManager em;

    // Required for CDI
    @Resource
    private UserTransaction utx;

    public void create(Medication medication) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Patient patientid = medication.getPatientid();
            if (patientid != null) {
                patientid = em.getReference(patientid.getClass(), patientid.getPatientid());
                medication.setPatientid(patientid);
            }
            em.persist(medication);
            if (patientid != null) {
                patientid.getMedicationList().add(medication);
                patientid = em.merge(patientid);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public void edit(Medication medication) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Medication persistentMedication = em.find(Medication.class, medication.getId());
            Patient patientidOld = persistentMedication.getPatientid();
            Patient patientidNew = medication.getPatientid();
            if (patientidNew != null) {
                patientidNew = em.getReference(patientidNew.getClass(), patientidNew.getPatientid());
                medication.setPatientid(patientidNew);
            }
            medication = em.merge(medication);
            if (patientidOld != null && !patientidOld.equals(patientidNew)) {
                patientidOld.getMedicationList().remove(medication);
                patientidOld = em.merge(patientidOld);
            }
            if (patientidNew != null && !patientidNew.equals(patientidOld)) {
                patientidNew.getMedicationList().add(medication);
                patientidNew = em.merge(patientidNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = medication.getId();
                if (findMedication(id) == null) {
                    throw new NonexistentEntityException("The medication with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Medication medication;
            try {
                medication = em.getReference(Medication.class, id);
                medication.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The medication with id " + id + " no longer exists.", enfe);
            }
            Patient patientid = medication.getPatientid();
            if (patientid != null) {
                patientid.getMedicationList().remove(medication);
                patientid = em.merge(patientid);
            }
            em.remove(medication);
            utx.commit();
        } catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Medication> findMedicationEntities() {
        return findMedicationEntities(true, -1, -1);
    }

    public List<Medication> findMedicationEntities(int maxResults, int firstResult) {
        return findMedicationEntities(false, maxResults, firstResult);
    }

    private List<Medication> findMedicationEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Medication.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Medication findMedication(Integer id) {
        return em.find(Medication.class, id);
    }

    public int getMedicationCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Medication> rt = cq.from(Medication.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
