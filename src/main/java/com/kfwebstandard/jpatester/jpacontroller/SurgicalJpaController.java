package com.kfwebstandard.jpatester.jpacontroller;

import com.kfwebstandard.jpatester.entities.Patient;
import com.kfwebstandard.jpatester.entities.Surgical;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.NonexistentEntityException;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generated controller updated to use CDI
 *
 * @author Ken
 */
@Named
@RequestScoped
public class SurgicalJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PatientJpaController.class);

    // Required for CDI
    @PersistenceContext(unitName = "hospitalPU")
    private EntityManager em;

    // Required for CDI
    @Resource
    private UserTransaction utx;

    public void create(Surgical surgical) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Patient patientid = surgical.getPatientid();
            if (patientid != null) {
                patientid = em.getReference(patientid.getClass(), patientid.getPatientid());
                surgical.setPatientid(patientid);
            }
            em.persist(surgical);
            if (patientid != null) {
                patientid.getSurgicalList().add(surgical);
                patientid = em.merge(patientid);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public void edit(Surgical surgical) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Surgical persistentSurgical = em.find(Surgical.class, surgical.getId());
            Patient patientidOld = persistentSurgical.getPatientid();
            Patient patientidNew = surgical.getPatientid();
            if (patientidNew != null) {
                patientidNew = em.getReference(patientidNew.getClass(), patientidNew.getPatientid());
                surgical.setPatientid(patientidNew);
            }
            surgical = em.merge(surgical);
            if (patientidOld != null && !patientidOld.equals(patientidNew)) {
                patientidOld.getSurgicalList().remove(surgical);
                patientidOld = em.merge(patientidOld);
            }
            if (patientidNew != null && !patientidNew.equals(patientidOld)) {
                patientidNew.getSurgicalList().add(surgical);
                patientidNew = em.merge(patientidNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = surgical.getId();
                if (findSurgical(id) == null) {
                    throw new NonexistentEntityException("The surgical with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Surgical surgical;
            try {
                surgical = em.getReference(Surgical.class, id);
                surgical.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The surgical with id " + id + " no longer exists.", enfe);
            }
            Patient patientid = surgical.getPatientid();
            if (patientid != null) {
                patientid.getSurgicalList().remove(surgical);
                patientid = em.merge(patientid);
            }
            em.remove(surgical);
            utx.commit();
        } catch (NotSupportedException | SystemException | NonexistentEntityException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Surgical> findSurgicalEntities() {
        return findSurgicalEntities(true, -1, -1);
    }

    public List<Surgical> findSurgicalEntities(int maxResults, int firstResult) {
        return findSurgicalEntities(false, maxResults, firstResult);
    }

    private List<Surgical> findSurgicalEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Surgical.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Surgical findSurgical(Integer id) {
        return em.find(Surgical.class, id);
    }

    public int getSurgicalCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Surgical> rt = cq.from(Surgical.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
