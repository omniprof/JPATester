package com.kfwebstandard.jpatester.jpacontroller;

import com.kfwebstandard.jpatester.jpacontroller.exceptions.NonexistentEntityException;
import com.kfwebstandard.jpatester.jpacontroller.exceptions.RollbackFailureException;
import com.kfwebstandard.jpatester.entities.Inpatient;
import com.kfwebstandard.jpatester.entities.Medication;
import com.kfwebstandard.jpatester.entities.Patient;
import com.kfwebstandard.jpatester.entities.Surgical;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generated controller updated to use CDI
 *
 * @author Ken
 */
@Named
@RequestScoped
public class PatientJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PatientJpaController.class);

    // Required for CDI
    @PersistenceContext(unitName = "hospitalPU")
    private EntityManager em;

    // Required for CDI
    @Resource
    private UserTransaction utx;

    public void create(Patient patient) throws RollbackFailureException, Exception {
        if (patient.getInpatientList() == null) {
            patient.setInpatientList(new ArrayList<>());
        }
        if (patient.getSurgicalList() == null) {
            patient.setSurgicalList(new ArrayList<>());
        }
        if (patient.getMedicationList() == null) {
            patient.setMedicationList(new ArrayList<>());
        }
        try {
            utx.begin();
            List<Inpatient> attachedInpatientList = new ArrayList<>();
            for (Inpatient inpatientListInpatientToAttach : patient.getInpatientList()) {
                inpatientListInpatientToAttach = em.getReference(inpatientListInpatientToAttach.getClass(), inpatientListInpatientToAttach.getId());
                attachedInpatientList.add(inpatientListInpatientToAttach);
            }
            patient.setInpatientList(attachedInpatientList);
            List<Surgical> attachedSurgicalList = new ArrayList<>();
            for (Surgical surgicalListSurgicalToAttach : patient.getSurgicalList()) {
                surgicalListSurgicalToAttach = em.getReference(surgicalListSurgicalToAttach.getClass(), surgicalListSurgicalToAttach.getId());
                attachedSurgicalList.add(surgicalListSurgicalToAttach);
            }
            patient.setSurgicalList(attachedSurgicalList);
            List<Medication> attachedMedicationList = new ArrayList<>();
            for (Medication medicationListMedicationToAttach : patient.getMedicationList()) {
                medicationListMedicationToAttach = em.getReference(medicationListMedicationToAttach.getClass(), medicationListMedicationToAttach.getId());
                attachedMedicationList.add(medicationListMedicationToAttach);
            }
            patient.setMedicationList(attachedMedicationList);
            em.persist(patient);
            for (Inpatient inpatientListInpatient : patient.getInpatientList()) {
                Patient oldPatientidOfInpatientListInpatient = inpatientListInpatient.getPatientid();
                inpatientListInpatient.setPatientid(patient);
                inpatientListInpatient = em.merge(inpatientListInpatient);
                if (oldPatientidOfInpatientListInpatient != null) {
                    oldPatientidOfInpatientListInpatient.getInpatientList().remove(inpatientListInpatient);
                    oldPatientidOfInpatientListInpatient = em.merge(oldPatientidOfInpatientListInpatient);
                }
            }
            for (Surgical surgicalListSurgical : patient.getSurgicalList()) {
                Patient oldPatientidOfSurgicalListSurgical = surgicalListSurgical.getPatientid();
                surgicalListSurgical.setPatientid(patient);
                surgicalListSurgical = em.merge(surgicalListSurgical);
                if (oldPatientidOfSurgicalListSurgical != null) {
                    oldPatientidOfSurgicalListSurgical.getSurgicalList().remove(surgicalListSurgical);
                    oldPatientidOfSurgicalListSurgical = em.merge(oldPatientidOfSurgicalListSurgical);
                }
            }
            for (Medication medicationListMedication : patient.getMedicationList()) {
                Patient oldPatientidOfMedicationListMedication = medicationListMedication.getPatientid();
                medicationListMedication.setPatientid(patient);
                medicationListMedication = em.merge(medicationListMedication);
                if (oldPatientidOfMedicationListMedication != null) {
                    oldPatientidOfMedicationListMedication.getMedicationList().remove(medicationListMedication);
                    oldPatientidOfMedicationListMedication = em.merge(oldPatientidOfMedicationListMedication);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public void edit(Patient patient) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Patient persistentPatient = em.find(Patient.class, patient.getPatientid());
            List<Inpatient> inpatientListOld = persistentPatient.getInpatientList();
            List<Inpatient> inpatientListNew = patient.getInpatientList();
            List<Surgical> surgicalListOld = persistentPatient.getSurgicalList();
            List<Surgical> surgicalListNew = patient.getSurgicalList();
            List<Medication> medicationListOld = persistentPatient.getMedicationList();
            List<Medication> medicationListNew = patient.getMedicationList();
            List<Inpatient> attachedInpatientListNew = new ArrayList<>();
            for (Inpatient inpatientListNewInpatientToAttach : inpatientListNew) {
                inpatientListNewInpatientToAttach = em.getReference(inpatientListNewInpatientToAttach.getClass(), inpatientListNewInpatientToAttach.getId());
                attachedInpatientListNew.add(inpatientListNewInpatientToAttach);
            }
            inpatientListNew = attachedInpatientListNew;
            patient.setInpatientList(inpatientListNew);
            List<Surgical> attachedSurgicalListNew = new ArrayList<>();
            for (Surgical surgicalListNewSurgicalToAttach : surgicalListNew) {
                surgicalListNewSurgicalToAttach = em.getReference(surgicalListNewSurgicalToAttach.getClass(), surgicalListNewSurgicalToAttach.getId());
                attachedSurgicalListNew.add(surgicalListNewSurgicalToAttach);
            }
            surgicalListNew = attachedSurgicalListNew;
            patient.setSurgicalList(surgicalListNew);
            List<Medication> attachedMedicationListNew = new ArrayList<>();
            for (Medication medicationListNewMedicationToAttach : medicationListNew) {
                medicationListNewMedicationToAttach = em.getReference(medicationListNewMedicationToAttach.getClass(), medicationListNewMedicationToAttach.getId());
                attachedMedicationListNew.add(medicationListNewMedicationToAttach);
            }
            medicationListNew = attachedMedicationListNew;
            patient.setMedicationList(medicationListNew);
            patient = em.merge(patient);
            for (Inpatient inpatientListOldInpatient : inpatientListOld) {
                if (!inpatientListNew.contains(inpatientListOldInpatient)) {
                    inpatientListOldInpatient.setPatientid(null);
                    inpatientListOldInpatient = em.merge(inpatientListOldInpatient);
                }
            }
            for (Inpatient inpatientListNewInpatient : inpatientListNew) {
                if (!inpatientListOld.contains(inpatientListNewInpatient)) {
                    Patient oldPatientidOfInpatientListNewInpatient = inpatientListNewInpatient.getPatientid();
                    inpatientListNewInpatient.setPatientid(patient);
                    inpatientListNewInpatient = em.merge(inpatientListNewInpatient);
                    if (oldPatientidOfInpatientListNewInpatient != null && !oldPatientidOfInpatientListNewInpatient.equals(patient)) {
                        oldPatientidOfInpatientListNewInpatient.getInpatientList().remove(inpatientListNewInpatient);
                        oldPatientidOfInpatientListNewInpatient = em.merge(oldPatientidOfInpatientListNewInpatient);
                    }
                }
            }
            for (Surgical surgicalListOldSurgical : surgicalListOld) {
                if (!surgicalListNew.contains(surgicalListOldSurgical)) {
                    surgicalListOldSurgical.setPatientid(null);
                    surgicalListOldSurgical = em.merge(surgicalListOldSurgical);
                }
            }
            for (Surgical surgicalListNewSurgical : surgicalListNew) {
                if (!surgicalListOld.contains(surgicalListNewSurgical)) {
                    Patient oldPatientidOfSurgicalListNewSurgical = surgicalListNewSurgical.getPatientid();
                    surgicalListNewSurgical.setPatientid(patient);
                    surgicalListNewSurgical = em.merge(surgicalListNewSurgical);
                    if (oldPatientidOfSurgicalListNewSurgical != null && !oldPatientidOfSurgicalListNewSurgical.equals(patient)) {
                        oldPatientidOfSurgicalListNewSurgical.getSurgicalList().remove(surgicalListNewSurgical);
                        oldPatientidOfSurgicalListNewSurgical = em.merge(oldPatientidOfSurgicalListNewSurgical);
                    }
                }
            }
            for (Medication medicationListOldMedication : medicationListOld) {
                if (!medicationListNew.contains(medicationListOldMedication)) {
                    medicationListOldMedication.setPatientid(null);
                    medicationListOldMedication = em.merge(medicationListOldMedication);
                }
            }
            for (Medication medicationListNewMedication : medicationListNew) {
                if (!medicationListOld.contains(medicationListNewMedication)) {
                    Patient oldPatientidOfMedicationListNewMedication = medicationListNewMedication.getPatientid();
                    medicationListNewMedication.setPatientid(patient);
                    medicationListNewMedication = em.merge(medicationListNewMedication);
                    if (oldPatientidOfMedicationListNewMedication != null && !oldPatientidOfMedicationListNewMedication.equals(patient)) {
                        oldPatientidOfMedicationListNewMedication.getMedicationList().remove(medicationListNewMedication);
                        oldPatientidOfMedicationListNewMedication = em.merge(oldPatientidOfMedicationListNewMedication);
                    }
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = patient.getPatientid();
                if (findPatient(id) == null) {
                    throw new NonexistentEntityException("The patient with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Patient patient;
            try {
                patient = em.getReference(Patient.class, id);
                patient.getPatientid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The patient with id " + id + " no longer exists.", enfe);
            }
            List<Inpatient> inpatientList = patient.getInpatientList();
            for (Inpatient inpatientListInpatient : inpatientList) {
                inpatientListInpatient.setPatientid(null);
                inpatientListInpatient = em.merge(inpatientListInpatient);
            }
            List<Surgical> surgicalList = patient.getSurgicalList();
            for (Surgical surgicalListSurgical : surgicalList) {
                surgicalListSurgical.setPatientid(null);
                surgicalListSurgical = em.merge(surgicalListSurgical);
            }
            List<Medication> medicationList = patient.getMedicationList();
            for (Medication medicationListMedication : medicationList) {
                medicationListMedication.setPatientid(null);
                medicationListMedication = em.merge(medicationListMedication);
            }
            em.remove(patient);
            utx.commit();
        } catch (NotSupportedException | SystemException | NonexistentEntityException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    public List<Patient> findPatientEntities() {
        return findPatientEntities(true, -1, -1);
    }

    public List<Patient> findPatientEntities(int maxResults, int firstResult) {
        return findPatientEntities(false, maxResults, firstResult);
    }

    private List<Patient> findPatientEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Patient.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    public Patient findPatient(Integer id) {
        return em.find(Patient.class, id);
    }

    public int getPatientCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Patient> rt = cq.from(Patient.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
